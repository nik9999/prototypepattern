﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern
{
    public abstract class DrawingObject
    {
        public abstract void Draw();
        public Color Color { get; set; }
        protected DrawingObject(DrawingObject other)
        {
            this.Color = other.Color;
        }
        protected DrawingObject(Color color) { this.Color = color; }
    }
}
