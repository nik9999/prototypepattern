using Microsoft.VisualStudio.TestTools.UnitTesting;
using PrototypePattern;
using System.Drawing;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestCloneCircle()
        {
            Circle TargetEntity = new Circle(Color.Red, 22);
            var ResultEntity = TargetEntity.MyClone();

            Assert.AreEqual(TargetEntity.Diameter, ResultEntity.Diameter);
        }

        [TestMethod]
        public void TestCloneSquare()
        {
            Square TargetEntity = new Square(Color.Green, 11);
            var ResultEntity = TargetEntity.MyClone();

            Assert.AreEqual(TargetEntity.Side, ResultEntity.Side);
        }

       
    }
}
