﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern
{
    public class Square : RectangularObject, ICloneable, IMyCloneable<Square>
    {
        public int Side { get; set; }
        public override void Draw()
        {
        }
        public Square(Square other)
            : base(other)
        {
            this.Side = other.Side;
        }
        public Square(Color color, int side)
            : base(color, side, side)
        {
            this.Side = side;
        }

        #region ICloneable IMyCloneable Members
        public Square Clone() { return new Square(this); }
        object ICloneable.Clone()
        {
            return Clone();
        }

        public Square MyClone()
        {
            return Clone();
        }
        #endregion

    }

}
