﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern
{
    public abstract class RectangularObject : DrawingObject
    {
        public int Width { get; set; }
        public int Height { get; set; }
        protected RectangularObject(RectangularObject other)
            : base(other)
        {
            Height = other.Height;
            Width = other.Width;
        }
        protected RectangularObject(Color color, int width, int height)
            : base(color)
        {
            this.Width = width;
            this.Height = height;
        }
    }
}
