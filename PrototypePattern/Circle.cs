﻿using System;
using System.Drawing;

namespace PrototypePattern
{
    public class Circle : RectangularObject, ICloneable, IMyCloneable<Circle>
    {
        public int Diameter { get; set; }
        public override void Draw()
        {
        }
        public Circle(Circle other)
            : base(other)
        {
            this.Diameter = other.Diameter;
        }
        public Circle(Color color, int diameter)
            : base(color, diameter, diameter)
        {
            Diameter = diameter;
        }

        #region ICloneable IMyCloneable Members
        public Circle Clone() { return new Circle(this); }
        

        object ICloneable.Clone()
        {
            return Clone();
        }

        public Circle MyClone()
        {
            return Clone();
        }
        #endregion

    }

}
